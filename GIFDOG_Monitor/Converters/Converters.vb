﻿#Region "Copyright Syncfusion Inc. 2001 - 2012"
' Copyright Syncfusion Inc. 2001 - 2012. All rights reserved.
' Use of this code is subject to the terms of our license.
' A copy of the current license can be obtained at any time by e-mailing
' licensing@syncfusion.com. Any infringement will be prosecuted under
' applicable laws. 
#End Region

Imports System.Windows.Data
Imports System.Windows

Public Class StyleConverter
    Implements IValueConverter
    Public Function Convert(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim window As MainWindow = TryCast(Application.Current.MainWindow, MainWindow)
        Dim basestyle As New Style()

        If window.Resources.MergedDictionaries.Count > 0 Then
            Dim rdic As ResourceDictionary = TryCast(window.MenuAdv.Resources.MergedDictionaries(window.MenuAdv.Resources.MergedDictionaries.Count - 1), ResourceDictionary)
            basestyle = TryCast(rdic(value.ToString() & "MenuItemAdvStyle"), Style)
            Dim itemcontainerstyle As Style = TryCast(window.Resources("menuItemAdvStyle"), Style)
            For Each setter As Setter In itemcontainerstyle.Setters
                basestyle.Setters.Add(setter)
            Next setter
        End If
        Return basestyle
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As System.Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New System.NotImplementedException()
    End Function


End Class

'Public Class InverseBooleanConverter
'    Implements IValueConverter

'    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
'        Return Not value
'    End Function

'    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
'        Return Not value
'    End Function
'End Class


