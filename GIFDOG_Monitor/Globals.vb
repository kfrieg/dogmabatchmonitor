﻿
Imports DogmaBatch_Monitor.AuftragContext

Public Class Globals
    
    Public Shared Conn As Devart.Data.Oracle.OracleConnection

    Public Shared WithEvents Auftraege As AuftragColl
    Public Shared Jobs As JobColl

    Public Shared CurCmds As New ApplicationCommands

    Public Shared CurAid As Long
    ' 1.0.3
    Public Shared CurTemplate As String
    Public Shared CurApplication As String
    Public Shared CurMandant As String
    Public Shared CurAuftrag As Auftrag

    Public Shared CurControls As New Controls



    ' Neuer DataContext
    'Public Shared GifDogDc As AuftragContext.AuftragDataContext
    Public Shared GifDogDb As AuftragDataContext
    
    Public Shared Sub ShowLog(Message As String)
        ' MsgBox(Message)
    End Sub

End Class
