﻿Imports System.Collections.ObjectModel
Imports System.Data
Imports DogmaBatch_Monitor.Globals
Imports DogmaBatch_Monitor.AuftragContext
Imports Devart.Data.Linq

' ReSharper disable CheckNamespace
Public Class AuftragColl
    ' ReSharper restore CheckNamespace
    Inherits ObservableCollection(Of Auftrag)

    Public Sub New()



        Try
            AddItems()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try




    End Sub

    Private TempJobCountList As New Dictionary(Of String, VWJOBSCOUNT)

    Private Sub ReadJobsCount()
        TempJobCountList.Clear()
        For Each cj As VWJOBSCOUNT In GifDogDb.VWJOBSCOUNTs
            TempJobCountList.Add(cj.AID, cj)
        Next
    End Sub


    Public Sub Refresh()
        ' GifDogDb.RejectChanges() nicht nötig, da OverwriteCurrentValues
        GifDogDb.Refresh(RefreshMode.OverwriteCurrentValues, GifDogDb.VWJOBSCOUNTs) ' Alle ?? Nein nur VWJOBSCOUNTs wird nicht refreshed
        Clear()
        AddItems()
    End Sub

    Private Function RechnerTest(lokal As String) As Boolean

        Select Case My.Settings.ComputerName
            Case "ALL"
                Return True
            Case ""
                Return String.IsNullOrEmpty(lokal) ' NULL
            Case Else
                Return lokal = My.Settings.ComputerName
        End Select

    End Function



    Private Sub AddItems()

        If IsNothing(GifDogDb) Then
            GifDogDb = New AuftragDataContext(Conn)
        End If
        ReadJobsCount()
        Try

            ' 1.0.8 Computername berücksichtigen
            For Each AufTragDef As GDAUFTRAGDEF In GifDogDb.GDAUFTRAGDEFs




                For Each Auftrag As GDAUFTRAG In AufTragDef.GDAUFTRAGs.Where(Function(d) RechnerTest(d.LOKAL))
                    ' Nur die ohne ServiceStatus Null jetzt im Dataset

                    Dim Offene As Long = 0
                    Dim ZuArchivierende As Long = 0
                    Dim Fehler As Long = 0



                    Try

                        ' Besser Ohne Relateion
                        'If Auftrag.VWJOBSCOUNTs.Any() Then
                        '    Offene = Auftrag.VWJOBSCOUNTs(0).OFFEN
                        '    ZuArchivierende = Auftrag.VWJOBSCOUNTs(0).NOCHNICHTARCHIVIERT
                        '    Fehler = Auftrag.VWJOBSCOUNTs(0).SAVEFEHLER + Auftrag.VWJOBSCOUNTs(0).GENFEHLER
                        'End If

                        ' Immer noch langsam
                        ' Zwischen Speichern
                        'If GifDogDb.VWJOBSCOUNTs.Count(Function(j) j.AID = Auftrag.AID) > 0 Then
                        '    Dim JobCount As VWJOBSCOUNT = GifDogDb.VWJOBSCOUNTs.First(Function(j) j.AID = Auftrag.AID)

                        '    Offene = JobCount.OFFEN
                        '    ZuArchivierende = JobCount.NOCHNICHTARCHIVIERT
                        '    Fehler = JobCount.SAVEFEHLER + JobCount.GENFEHLER
                        'End If

                        If TempJobCountList.ContainsKey(Auftrag.AID) Then


                            Offene = TempJobCountList(Auftrag.AID).OFFEN
                            ZuArchivierende = TempJobCountList(Auftrag.AID).NOCHNICHTARCHIVIERT
                            Fehler = TempJobCountList(Auftrag.AID).SAVEFEHLER + TempJobCountList(Auftrag.AID).GENFEHLER
                        End If
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                    CurAuftrag = New Auftrag(AufTragDef, Auftrag, Offene, ZuArchivierende, Fehler)
                    ' Keine Jobs mehr
                    Add(CurAuftrag)
                Next
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    'Private Sub AddItems_Old()

    '    Dim CurAuftrag As Auftrag

    '    Dim RelAuftragDefAuftrag As DataRelation
    '    Dim RelAuftragJobCount As DataRelation

    '    Try
    '        Globals.ShowLog("Init Dataset")
    '        GifDog = New GifDogDs
    '        Globals.ShowLog("Init Dataset erfolgreich")

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    RelAuftragDefAuftrag = GifDog.Relations("GD_FK_AUFTRAG_ADID")
    '    RelAuftragJobCount = GifDog.Relations("GD_AUFTRAG_VW_JOBSCOUNT")
    '    Globals.ShowLog("Init Relation erfolgreich")


    '    Try
    '        With ADTa
    '            .Connection = Globals.Conn
    '            .Fill(GifDog.GD_AUFTRAG_DEF)
    '        End With
    '        Globals.ShowLog("Init GD_AUFTRAG_DEF erfolgreich")
    '        With AUTa
    '            .Connection = Globals.Conn
    '            .Fill(GifDog.GD_AUFTRAG)
    '        End With
    '        Globals.ShowLog("Init GD_AUFTRAG erfolgreich")

    '        With JOCountTa
    '            .Connection = Globals.Conn
    '            .Fill(GifDog.VW_JOBSCOUNT)

    '        End With
    '        Globals.ShowLog("Init VW_JOBSCOUNT erfolgreich")
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try

    '    Try

    '        For Each AufTragDefRow As GifDogDs.GD_AUFTRAG_DEFRow In GifDog.GD_AUFTRAG_DEF
    '            For Each AuftragRow As GifDogDs.GD_AUFTRAGRow In AufTragDefRow.GetChildRows(RelAuftragDefAuftrag)
    '                ' Nur die ohne ServiceStatus Null jetzt im Dataset

    '                Dim Offene As Long = 0
    '                Dim ZuArchivierende As Long = 0
    '                Dim Fehler As Long = 0

    '                Try
    '                    For Each JobCount As GifDogDs.VW_JOBSCOUNTRow In AuftragRow.GetChildRows(RelAuftragJobCount)
    '                        Offene = JobCount.OFFEN
    '                        ZuArchivierende = JobCount.NOCHNICHTARCHIVIERT
    '                        Fehler = JobCount.SAVEFEHLER + JobCount.GENFEHLER
    '                    Next
    '                Catch ex As Exception
    '                    MsgBox(ex.Message)
    '                End Try

    '                CurAuftrag = New Auftrag(AufTragDefRow, AuftragRow, Offene, ZuArchivierende, Fehler)
    '                ' Keine Jobs mehr
    '                Add(CurAuftrag)
    '            Next
    '        Next
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try


    'End Sub



End Class
