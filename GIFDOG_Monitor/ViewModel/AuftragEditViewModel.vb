﻿Imports System.Collections.ObjectModel
Imports Thinktecture.DataForm
Friend Class AuftragEditViewModel(Of TEntity As ErrorInfoClass) 'TODO: ??

    Inherits ViewModelBase

    Private _curWindow As Window '  SingleEditView

    ' Commands
    Private _closeCommand As DelegateCommand
    Private _cancelCommand As DelegateCommand
    Private _actionCommand As ActionCommand

    ' Error Handling
    Private _hasError As Boolean
    Public IsInsert As Boolean

    Public Cancelled As Boolean


    ' Die Datenklasse

    ' Die Liste der Datenklassen
    ' TODO: Muss das eine  ObservableCollection sein ?? ja sonst kein PropertyChanged => keine DataListÄnderungen sichtbar
    Private _dataList As ObservableCollection(Of TEntity)

    Public Sub Show()
        ' ActionCommand.ExecuteAction(DataFormAction.Reset, Nothing)
        Try
            _curWindow.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


    End Sub

    Public Sub New(curWindow As Window)
        _curWindow = curWindow


        ' Init Commands
        CloseCommand = New DelegateCommand(AddressOf CanExecuteCommand, AddressOf ExecuteCloseCommand)
        CancelCommand = New DelegateCommand(AddressOf CanExecuteCancelCommand, AddressOf ExecuteCancelCommand)
        ' Eigentlich nicht genutzt
        _actionCommand = New ActionCommand()

        DataList = New ObservableCollection(Of TEntity) ' Dieser Befehl dauert lange
        _curWindow.DataContext = Me
    End Sub

    Public Sub Initialize(curauftrag As TEntity, curIsInsert As Boolean)

        IsInsert = curIsInsert

        DataList.Clear()
        DataList.Add(curauftrag)


        Cancelled = False
    End Sub

    'Private Sub ExecuteCloseCommand(ByVal obj As Object)
    '    MsgBox("Hallo")
    '    _curWindow.Hide()
    'End Sub


#Region "Command - Actions"

    Private Sub ExecuteCancelCommand(ByVal obj As Object)
        Cancelled = True
        _curWindow.Hide()
    End Sub

    Private Sub ExecuteCloseCommand(ByVal obj As Object)

        ActionCommand.ExecuteAction(DataFormAction.AcceptChanges, Nothing)


        '' TODO: HasError beim ersten Mal nicht aktuelisiert
        '' Hier ist bei Ora das Speichern erforderlich
        If HasError Or DataList(0).Error <> "" Then
            MsgBox(DataList(0).Error)
        Else
            _curWindow.Hide()


        End If



    End Sub

    Private Function CanExecuteCancelCommand(ByVal arg As Object) As Boolean
        Return True ' IsInsert

    End Function

    ' Für Alle 
    Private Function CanExecuteCommand(ByVal arg As Object) As Boolean
        Return True
    End Function
    'Private Function CanExecuteCommand(ByVal obj As Object) As Boolean
    '    Return True
    'End Function

    Public Property CloseCommand As DelegateCommand
        Get
            Return _closeCommand
        End Get
        Set(value As DelegateCommand)
            _closeCommand = value
            Me.OnPropertyChanged("CloseCommand")
        End Set
    End Property

    Public Property CancelCommand As DelegateCommand
        Get
            Return _cancelCommand
        End Get
        Set(value As DelegateCommand)
            _cancelCommand = value
            Me.OnPropertyChanged("CancelCommand")
        End Set
    End Property
    Public Property HasError As Boolean
        Get
            Return _hasError
        End Get
        Set(value As Boolean)
            _hasError = value
            Me.OnPropertyChanged("HasError")
        End Set
    End Property

    Public Property DataList As ObservableCollection(Of TEntity)
        Get
            Return _dataList
        End Get
        Set(value As ObservableCollection(Of TEntity))
            _dataList = value
            Me.OnPropertyChanged("DataList")
        End Set
    End Property

    Public Property ActionCommand As ActionCommand
        Get
            Return _actionCommand
        End Get
        Set(value As ActionCommand)
            _actionCommand = value
            Me.OnPropertyChanged("ActionCommand")
        End Set
    End Property

#End Region
End Class
