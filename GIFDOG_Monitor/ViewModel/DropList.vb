﻿Public Class DropList
    Private _Display As String
    Private _Value As String

    Public Property Display As String
        Get
            Return _Display
        End Get
        Set(curdisplay As String)
            _Display = curdisplay
        End Set
    End Property

    Public Property Value As String
        Get
            Return _Value
        End Get
        Set(curvalue As String)
            _Value = curvalue
        End Set
    End Property
End Class
