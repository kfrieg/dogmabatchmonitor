﻿Imports System.ComponentModel

Public Class Controls
    Implements INotifyPropertyChanged
    Private _toSave As Boolean

    Public Property ToSave As Boolean
        Get
            Return _toSave ' Globals.GifDog.HasChanges - nur über events
        End Get
        Set(value As Boolean)
            _ToSave = value
            OnPropertyChanged("ToSave")
        End Set
    End Property


    Protected Overridable Sub OnPropertyChanged(ByVal PropertyName As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(PropertyName))
    End Sub
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
End Class
