﻿Imports System.Collections.ObjectModel
Imports System.Data
Imports DogmaBatch_Monitor.Globals
Imports DogmaBatch_Monitor.AuftragContext

Public Class JobColl
    Inherits ObservableCollection(Of Job)
    Private _Aid As Long

    Public Sub New(CurAid As Long)
        _Aid = CurAid
        Try
            AddItems(CurAid)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try



    End Sub

    Public Property Aid As Long
        Get
            Return _Aid
        End Get
        Set(value As Long)
            _Aid = value
        End Set
    End Property

    Public Sub Refresh() 'CurAid As Long)
        Clear()
        AddItems(CurAid)
    End Sub

    Private Sub AddItems(CurAid As Long)

        Dim CurJob As Job
        'Try
        '    With JobsTa
        '        .Connection = Globals.Conn
        '        .Fill(GifDog.GD_DOGMAJOBS)
        '    End With
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try

        For Each JobRow As GDDOGMAJOB In GifDogDb.GDDOGMAJOBs.Where(Function(j) j.AID = CurAid)
            With JobRow
                ' 1.0.3
                Dim FM As String
                Dim FS As String
                Dim PI As String
                FM = Replace(.FEHLERMELDUNG, vbCrLf, " - ")
                FS = .FEHLERSTATUS
                CurJob = New Job(.PJOBID, .STATUS, .STATUSDATUM, FS, FM, .ERSTELLT)


            End With
            Add(CurJob)
        Next


    End Sub
End Class
