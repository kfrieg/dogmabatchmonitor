﻿Imports System.Data
Imports DogmaBatch_Monitor.AuftragContext
Imports Gat.Controls
Imports DogmaBatch_Monitor.MassenDruck
Imports System.IO
Imports System.Data.OleDb
Imports Devart.Data.Oracle
Imports Syncfusion.Linq
Imports Syncfusion.Windows.Data
Imports DogmaBatch_Monitor.Globals
Imports Syncfusion.Windows.Controls.Grid

Public Class ApplicationCommands
    Implements ICommand
    Implements IDisposable

    Private AEViewModel As AuftragEditViewModel(Of AuftragEdit)
    Private AEView As AuftragEditView
    Private _grid As GridDataControl

    Public WriteOnly Property Grid() As GridDataControl
        Set(ByVal value As GridDataControl)
            _grid = value
        End Set
    End Property

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged(sender As Object, e As System.EventArgs) Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute
        Select Case parameter
            Case "Info"
                Dim AboutWin As New About
                AboutWin.Title = "DogmaBatch Monitor"
                AboutWin.ApplicationLogo = New BitmapImage(New System.Uri("pack://application:,,,/Images/Printer2.ico"))
                AboutWin.AdditionalNotes = String.Format("Oracle-Datenbank: Schema: {0} SID: {1} Server: {2}", My.Settings.OraUser, My.Settings.OraSID, My.Settings.OraServer)
                AboutWin.Copyright = ""
                AboutWin.Show()

            Case "Exit"
                If Not IsNothing(AEViewModel) Then AEViewModel.Dispose()
                OraDb.CloseDb()
                End

                ' 1.0.7
                ' Dispose und init TODO:

            Case "NewAuftrag"

                If IsNothing(AEView) Then
                    AEView = New AuftragEditView()
                End If
                If IsNothing(AEViewModel) Then AEViewModel = New AuftragEditViewModel(Of AuftragEdit)(AEView)
                Dim CurAp As New AuftragEdit
                CurAp.SetNewDefaults()
                AEViewModel.Initialize(CurAp, False)

                AEViewModel.Show()
                If Not AEViewModel.Cancelled Then
                    Dim cad As AuftragContext.GDAUFTRAGDEF = CurAp.GetAuftragDef()
                    Dim cau As AuftragContext.GDAUFTRAG = CurAp.GetAuftrag()
                    cad.GDAUFTRAGs.Add(cau)
                    GifDogDb.GDAUFTRAGDEFs.InsertOnSubmit(cad)
                    GifDogDb.SubmitChanges()
                    Auftraege.Refresh()
                End If
            Case "DelAuftrag"
                If MsgBox("Auftrag löschen?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then


                    Dim query = From ad In GifDogDb.GDAUFTRAGDEFs Where ad.ADID = CurAuftrag.Adid Select ad
                    GifDogDb.GDAUFTRAGs.DeleteOnSubmit(query.First().GDAUFTRAGs(0))
                    GifDogDb.GDAUFTRAGDEFs.DeleteOnSubmit(query.FirstOrDefault())
                    GifDogDb.SubmitChanges()
                    Auftraege.Refresh()
                End If
            Case "EditAuftrag"

                If IsNothing(AEView) Then AEView = New AuftragEditView()
                If IsNothing(AEViewModel) Then AEViewModel = New AuftragEditViewModel(Of AuftragEdit)(AEView)
                Dim CurAp As New AuftragEdit
                Dim CurEditAuftrag As AuftragContext.GDAUFTRAGDEF
                Dim query = From ad In GifDogDb.GDAUFTRAGDEFs Where ad.ADID = CurAuftrag.Adid Select ad
                CurEditAuftrag = query.FirstOrDefault()

                CurAp.EditAuftragDef(CurEditAuftrag)


                AEViewModel.Initialize(CurAp, True)
                AEViewModel.Show()
                If Not AEViewModel.Cancelled Then


                    CurEditAuftrag = CurAp.GetAuftragDef(CurEditAuftrag)
                    'CurEditAuftrag.GDAUFTRAGs(0) = CurAp.GetAuftrag(CurEditAuftrag.GDAUFTRAGs(0)) ' ???

                    With CurEditAuftrag.GDAUFTRAGs(0)
                        .SERVICESTATUS = CurAp.ServiceStatus
                        .KNGIFSTATUS = "F"
                        .OFFEN = BoolToString(CurAp.Offen)
                        .KNARCHIV = BoolToString(CurAp.Archiv)
                        .KNDRUCK = BoolToString(CurAp.Druck)
                        .KNFAXEMAIL = CurAp.FaxEmail
                        .KNMASSENDRUCK = BoolToString(CurAp.Massendruck)
                        .LOKAL = CurAp.Lokal
                    End With

                    GifDogDb.SubmitChanges()
                    Auftraege.Refresh()
                End If



            Case "JobWindow"
                If Globals.Jobs Is Nothing Then
                    Globals.Jobs = New JobColl(CurAid)
                Else
                    If Jobs.Aid <> CurAid Then
                        Globals.Jobs = New JobColl(CurAid)
                    Else
                        Jobs.Refresh()

                    End If
                End If
                Dim jobWindow As New JobsWindow()
                jobWindow.ShowDialog()

            Case "Speichern"
                If GifDogDb.GetChangeSet().Updates.Count > 0 Then

                    If MsgBox("Änderungen speichern?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        Try

                            GifDogDb.SubmitChanges()
                            CurControls.ToSave = False

                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try

                    End If
                End If


                'If GifDog.HasChanges Then
                '    If MsgBox("Änderungen speichern?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                '        Try
                '            ADTa.Update(GifDog)
                '            AUTa.Update(GifDog)
                '            JobsTa.Update(GifDog)
                '            CurControls.ToSave = False

                '        Catch ex As Exception
                '            MsgBox(ex.Message)
                '        End Try

                '    End If
                'End If
            Case "Refresh"
                Auftraege.Refresh()
            Case "RefreshJobs"
                Jobs.Refresh() ' CurAid)
            Case "ClearFilter"



                For Each visibleColumn In _grid.VisibleColumns
                    If Not visibleColumn.Filters Is Nothing Then
                        Try
                            If visibleColumn.Filters.Count > 0 Then visibleColumn.Filters.Clear()
                        Catch ex As Exception

                        End Try

                    End If

                Next


            Case "ActivateAll"
                ' Model.View.Records enthält die gefilterten
                For Each CurRow In _grid.Model.View.Records
                    Dim CurAuftrag As DogmaBatch_Monitor.Auftrag = DirectCast(DirectCast(CurRow, Syncfusion.Windows.Controls.Grid.GridDataRecord).Data, DogmaBatch_Monitor.Auftrag)
                    CurAuftrag.ServiceStatus = "A"
                Next

                ' Filter löschen > 0 - Filter auf Aktiv Setzen

                If MsgBox("Änderungen speichern?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                    Try
                        ' AUTa.Update(GifDog)
                        GifDogDb.SubmitChanges()
                        CurControls.ToSave = False

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try


                    Dim AktivPred As New FilterPredicate
                    AktivPred.FilterType = FilterType.Equals
                    AktivPred.FilterValue = "A"
                    _grid.VisibleColumns("Anzahl").Filters.Clear()
                    _grid.VisibleColumns("ServiceStatus").Filters.Add(AktivPred)
                Else
                    Auftraege.Refresh()
                End If

            Case "DeActivateAll"


                'For Each CurAuftrag As GifDogDs.GD_AUFTRAGRow In GifDog.GD_AUFTRAG
                '    CurAuftrag.SERVICESTATUS = "N"
                'Next
                '(New System.Collections.Generic.Mscorlib_CollectionDebugView(Of GIFDOG_Monitor.Auftrag)(DirectCast(_Grid.ItemsSource,System.Windows.Data.CollectionViewSource).Source)).Items(0)

                ' Besser im Grid
                For Each visibleColumn In _grid.VisibleColumns
                    If Not visibleColumn.Filters Is Nothing Then
                        visibleColumn.Filters.Clear()
                    End If

                Next

                ' ItemsSource enthält alle
                For Each CurRow As DogmaBatch_Monitor.Auftrag In DirectCast(_grid.ItemsSource, System.Windows.Data.CollectionViewSource).Source
                    Debug.Print(CurRow.ServiceStatus)
                    CurRow.ServiceStatus = "N"

                Next
                If MsgBox("Änderungen speichern?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    Try
                        GifDogDb.SubmitChanges()
                        'AUTa.Update(GifDog)
                        CurControls.ToSave = False

                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                Else
                    Auftraege.Refresh()
                End If

            Case "CopyLeasitJobs"

                Dim OraCmd As New Devart.Data.Oracle.OracleCommand()
                Try
                    OraCmd.Connection = Globals.Conn
                    OraCmd.CommandTimeout = 1200
                    OraCmd.CommandType = CommandType.StoredProcedure
                    OraCmd.CommandText = "gd_dogmabatch.copyleaseitjobs"

                    OraCmd.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    OraCmd.Dispose()
                End Try

                MsgBox("Ausführung beendet.")

                Auftraege.Refresh()

            Case "ResetFehlerStatus"
                Dim AktAid As Long = CurAid
                If AktAid <> 0 Then
                    'Dim RelAuftragJob As DataRelation = GifDog.Relations("GD_AUFTRAG_GD_DOGMAJOBS")
                    For Each JobRow As GDDOGMAJOB In GifDogDb.GDAUFTRAGs.Where(Function(a) a.AID = CurAid).First().GDDOGMAJOBs

                        With JobRow
                            If JobRow.FEHLERMELDUNG <> "" Then JobRow.FEHLERMELDUNG = ""
                            If JobRow.FEHLERSTATUS <> "N" Then JobRow.FEHLERSTATUS = "N"
                        End With
                    Next
                    Dim CurAuftrag As Auftrag = Auftraege.Where(Function(a) a._Aid = AktAid).FirstOrDefault

                    For Each CurJob As Job In CurAuftrag.JobList
                        CurJob.Fehlermeldung = ""
                        CurJob.FehlerStatus = "N"
                    Next

                    If GifDogDb.GetChangeSet().Updates.Count > 0 Then
                        Try

                            GifDogDb.SubmitChanges()
                            Jobs.Refresh() ' CurAid)

                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    End If

                End If
            Case "ImportMassendruck" ' 1.0.3

                If CurApplication = "MASSENDRUCK" Then
                    Dim MDImp As New MDImport

                    If MDImp.Import(CurAid, CurTemplate, CurMandant) Then
                        Auftraege.Refresh()
                        MsgBox("Import erfolgreich!", MsgBoxStyle.Information)
                    End If
                Else
                    MsgBox("Der Import kann nur in MASSENDRUCK-Aufträge erfolgen.", MsgBoxStyle.Critical)
                End If
            Case "ExportMassendruckXML" '1.0.4

                Dim dlg As New Microsoft.Win32.OpenFileDialog()
                dlg.DefaultExt = ".xlsx"
                dlg.Filter = "Excel Dateien (.xlsx)|*.xlsx"
                Dim result As Nullable(Of Boolean) = dlg.ShowDialog()

                ' Konvertieren
                If result = True Then
                    ' Open document 
                    Dim filename As String = dlg.FileName
                    Dim Curdir As String = My.Computer.FileSystem.GetFileInfo(filename).Directory.FullName

                    Dim ExcelImp As New ExcelImport(filename, 0, 0)
                    Try
                        ExcelImp.GenerateData()

                        If ExcelImp.RowErrors.ToString() <> "" Then
                            MsgBox("Fehler in folgenden Zeilen:" & vbCrLf & ExcelImp.RowErrors.ToString(), MsgBoxStyle.Critical)
                        End If
                        ' Ausgabe
                        Using sr As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(IO.Path.Combine(Curdir, "DatenSatz01.xml"), False)
                            sr.Write(ExcelImp.JobDataList(0).Daten)
                        End Using

                        ' Meldung
                        MsgBox(String.Format("Datei {0} exportiert.", IO.Path.Combine(Curdir, "DatenSatz01.xml")))
                    Catch ex As Exception
                        MsgBox(String.Format("Import-Fehler: {0}", ex.Message))
                    End Try



                End If
        End Select
    End Sub
    Private Function BoolToString(inVal As Boolean) As String
        If inVal Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function


    '        Case "EditAuftrag2"
    '' CurAid, CurTemplate, CurMandant
    ''Dim AEView As New AuftragEdit
    ''AEView.ShowDialog()

    'Dim CurAp As New AuftragEdit

    'Dim AEView As New AuftragEditViewModel(Of AuftragEdit)
    '            AEView.Initialize(CurAp, False, New AuftragEditView)
    '            AEView.Show()



    'Dim dt As GifDogDs.GD_AUFTRAG_DEFDataTable = GifDog.GD_AUFTRAG_DEF
    'Dim dr As GifDogDs.GD_AUFTRAG_DEFRow
    '            dr = dt.NewRow()
    'Dim dt2 As GifDogDs.GD_AUFTRAGDataTable = GifDog.GD_AUFTRAG
    'Dim dr2 As GifDogDs.GD_AUFTRAGRow
    '            dr2 = dt2.NewRow()

    '' Autoincrement gesetzt
    '            With CurAp
    '' dr.ADID = 0
    '                dr.ADSCHL = "Test"
    '                dr.APPLICATION = .Application
    '                dr.BEZEICHNUNG = .Bezeichnung
    '                dr.QUEUEID = 10769984
    '                dr.QUELLE = "GD_DOGMAJOBS"
    '' Changedby/on
    '                dr.SORT = "" ' XX
    '                dr.MANDANT = .Mandant
    ''dr.AUSGABEVZ = '

    '' dr2.AID = 0
    '                dr2.SERVICESTATUS = "N"
    '                dr2.KN_GIFSTATUS = "F"
    '                dr2.OFFEN = "Y"

    '            End With

    ''dr2.GD_AUFTRAG_DEFRow = dr

    ''Dim oc As New OracleCommand

    ''oc.ExecuteScalar()

    '            dt.AddGD_AUFTRAG_DEFRow(dr)
    ''GifDog.GD_AUFTRAG_DEF.AddGD_AUFTRAG_DEFRow(dr)

    '            AddHandler ADTa.Adapter.RowUpdated, New OracleRowUpdatedEventHandler(AddressOf ADTA_RowUpDating)



    '            ADTa.Update(GifDog)

    'Dim oc As New OracleCommand()
    '            oc.Connection = Conn
    '            oc.CommandText = "select max(ADID) as X from gd_auftrag_def"
    'Dim cor As OracleDataReader = oc.ExecuteReader()

    'Dim adid As Double
    '            While cor.Read()
    '                adid = cor.GetDouble("X")
    '            End While



    ''While oc.ExecuteReader()

    ''End While


    'Dim dt5 As DataTable = GifDog.GD_AUFTRAG_DEF.GetChanges()
    ''GifDog.GetChanges()

    '            For Each crr As GifDogDs.GD_AUFTRAG_DEFRow In GifDog.GD_AUFTRAG_DEF.Rows

    '            Next



    '            dr.ADID = adid
    '            dr2.ADID = adid ' DirectCast(GifDog.GD_AUFTRAG_DEF.Rows(GifDog.GD_AUFTRAG_DEF.Rows.Count - 1), DogmaBatch_Monitor.GifDogDs.GD_AUFTRAG_DEFRow).ADID
    '            MsgBox(dr2.ADID)

    '            GifDog.GD_AUFTRAG.AddGD_AUFTRAGRow(dr2)


    '            AUTa.Update(GifDog)

    '' MsgBox(dr.ADID)
    '' MsgBox(dr2.ADID)


    '' TEst Änderung der Application
    '' so geht's dann
    ''Dim currow As GifDogDs.GD_AUFTRAG_DEFRow
    ''currow = GifDog.GD_AUFTRAG_DEF.Rows(1)
    ''currow.APPLICATION = "Test3"
    ''ADTa.Update(GifDog)

    ''currow = GifDog.GD_AUFTRAG_DEF.AddGD_AUFTRAG_DEFRow(0, "XX", "YY", "", 0, "", "", "", "", "zz")
    ''                ADTa.Update(GifDog)

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If Not IsNothing(AEViewModel) Then AEViewModel.Dispose()
            End If

        End If
        Me.disposedValue = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
