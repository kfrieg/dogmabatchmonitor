﻿'Imports Devart.Data
Imports Devart.Data.Oracle


Public Class OraDb
    Private Shared _conn As OracleConnection

    Public Shared ReadOnly Property Conn As OracleConnection
        Get
            Return _conn
        End Get
    End Property

    Public Shared Function OpenDb() As Boolean
        Dim OraBuilder As New OracleConnectionStringBuilder
        With OraBuilder
            .UserId = My.Settings.OraUser
            .Password = My.Settings.OraPWD
            .Direct = True
            .Server = My.Settings.OraServer
            .Sid = My.Settings.OraSID
        End With
        _conn = New OracleConnection(OraBuilder.ConnectionString)

        Try
            Conn.Open()
            Return True
        Catch ex As Exception
            MsgBox(String.Format("Verbindung zu {0} auf Server {1} mit Sid {2} kann nicht aufgebaut werden. ({3})", OraBuilder.UserId, OraBuilder.Server, OraBuilder.Sid, ex.Message))
            Return False
        End Try


    End Function

    Public Shared Sub CloseDb()
        Conn.Close()
        Conn.Dispose()
    End Sub

End Class
