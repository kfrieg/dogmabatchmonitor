﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Thinktecture.DataForm

'<MetadataType(GetType(AuftragEditMetaData))> _
Public Class AuftragEdit
    Inherits ErrorInfoClass
    Implements INotifyPropertyChanged
    Implements IDataErrorInfo

    Private _vorlage As String

    Private _serviceStatus As String

    Private _offen As Boolean

    Private _druck As Boolean

    Private _massendruck As Boolean
    Private _archiv As Boolean
    Private _faxEmail As String
    Private _bezeichnung As String

    Private _mandant As String

    Private _application As String

    Private _queueid As Integer

    Private _sort As String

    Private _ausgabeverz As String

    Private _lokal As String

    Public Sub SetNewDefaults()
        _offen = True
        _serviceStatus = "N"
        _druck = False
        _massendruck = False
        _archiv = False
        _faxEmail = "N"

    End Sub

    Public Function GetAuftragDef() As AuftragContext.GDAUFTRAGDEF
        Return GetAuftragDef(New AuftragContext.GDAUFTRAGDEF)
    End Function

    Public Function GetAuftragDef(cad As AuftragContext.GDAUFTRAGDEF) As AuftragContext.GDAUFTRAGDEF

        With cad
            .APPLICATION = _application
            .BEZEICHNUNG = _bezeichnung
            .VORLAGE = _vorlage
            .ADSCHL = _vorlage
            .QUEUEID = 10769984
            .QUELLE = "GD_DOGMAJOBS"
            .CHANGEDBY = Environment.UserName
            .CHANGEDON = Now
            .SORT = _sort
            .MANDANT = _mandant
            .AUSGABEVZ = _ausgabeverz
        End With
        ' TODO:

        Return cad
    End Function

    Public Function GetAuftrag() As AuftragContext.GDAUFTRAG
        Return GetAuftrag(New AuftragContext.GDAUFTRAG)
    End Function

    Public Function GetAuftrag(cau As AuftragContext.GDAUFTRAG) As AuftragContext.GDAUFTRAG
        With cau
            .KNGIFSTATUS = "F"
            .SERVICESTATUS = _serviceStatus
            .OFFEN = BoolToString(_offen)
            .KNARCHIV = BoolToString(_archiv)
            .KNDRUCK = BoolToString(_druck)
            .KNFAXEMAIL = _faxEmail
            .KNMASSENDRUCK = BoolToString(_massendruck)
            .LOKAL = _lokal
        End With

        Return cau
    End Function

    Public Sub EditAuftragDef(inAuftragDef As AuftragContext.GDAUFTRAGDEF)
        With inAuftragDef
            ' TODO:
            _application = .APPLICATION
            _bezeichnung = .BEZEICHNUNG
            _vorlage = .VORLAGE
            _queueid = .QUEUEID
            _sort = .SORT
            _mandant = .MANDANT
            _ausgabeverz = .AUSGABEVZ

            _serviceStatus = .GDAUFTRAGs(0).SERVICESTATUS
            _offen = StringToBool(.GDAUFTRAGs(0).OFFEN)
            _archiv = StringToBool(.GDAUFTRAGs(0).KNARCHIV)
            _druck = StringToBool(.GDAUFTRAGs(0).KNDRUCK)
            _faxEmail = .GDAUFTRAGs(0).KNFAXEMAIL
            _massendruck = StringToBool(.GDAUFTRAGs(0).KNMASSENDRUCK)
            _lokal = .GDAUFTRAGs(0).LOKAL


        End With

    End Sub

    Private Function BoolToString(inVal As Boolean) As String
        If inVal Then
            Return "Y"
        Else
            Return "N"
        End If
    End Function
    Private Function StringToBool(inVal As String) As Boolean
        Return inVal = "Y"
    End Function


    ' TODO: Fehlerhandling
    <Display(AutoGenerateField:=False)>
    Default Public Overrides ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Dim ErrorText As String = String.Empty

            ' Fehlerbehandlung für die einzelnen Spalten
            If String.IsNullOrEmpty(columnName) OrElse columnName = "Vorlage" Then
                If String.IsNullOrEmpty(Me.Vorlage) Then
                    ErrorText += "Vorlage ist Pflichtfeld" + Environment.NewLine
                End If
            End If
            If String.IsNullOrEmpty(columnName) OrElse columnName = "Application" Then
                If String.IsNullOrEmpty(Me.Application) Then
                    ErrorText += "Application ist Pflichtfeld" + Environment.NewLine
                End If
            End If
            If String.IsNullOrEmpty(columnName) OrElse columnName = "Mandant" Then
                If String.IsNullOrEmpty(Me.Mandant) Then
                    ErrorText += "Mandant ist Pflichtfeld" + Environment.NewLine
                End If
            End If
                Return ErrorText
        End Get
    End Property


   
    <Required()>
    <LocalizableDisplayFormat(NullDisplayText:="Eingabe einer Application erforderlich")>
    Public Property Application As String
        Get
            Return _application
        End Get
        Set(value As String)
            _application = value
            OnPropertyChanged("Application")
        End Set
    End Property
    <Required()>
    Public Property Vorlage() As String
        Get
            Return _vorlage
        End Get
        Set(value As String)
            _vorlage = value
            OnPropertyChanged("Vorlage")
        End Set
    End Property
    Public Property Bezeichnung As String
        Get
            Return _bezeichnung
        End Get
        Set(value As String)
            _bezeichnung = value
            OnPropertyChanged("Bezeichnung")
        End Set
    End Property
    <Required()>
    Public Property Mandant As String
        Get
            Return _mandant
        End Get
        Set(value As String)
            _mandant = value
            OnPropertyChanged("Mandant")
        End Set
    End Property


    Public Property ServiceStatus As String
        Get
            Return _serviceStatus
        End Get
        Set(value As String)
            _serviceStatus = value
            OnPropertyChanged("ServiceStatus")
        End Set
    End Property

    Public Property Offen As Boolean
        Get
            Return _offen
        End Get
        Set(value As Boolean)
            _offen = value
            OnPropertyChanged("Offen")
        End Set
    End Property

    Public Property Druck As Boolean
        Get
            Return _druck
        End Get
        Set(value As Boolean)
            _druck = value
            OnPropertyChanged("Druck")
        End Set
    End Property

    Public Property Massendruck As Boolean
        Get
            Return _massendruck
        End Get
        Set(value As Boolean)
            _massendruck = value
            OnPropertyChanged("Massendruck")
        End Set
    End Property

    Public Property Archiv As Boolean
        Get
            Return _archiv
        End Get
        Set(value As Boolean)
            _archiv = value
            OnPropertyChanged("Archiv")
        End Set
    End Property

    Public Property FaxEmail As String
        Get
            Return _faxEmail
        End Get
        Set(value As String)
            _faxEmail = value
            OnPropertyChanged("FaxEmail")
        End Set
    End Property
    Public Property Queueid As Integer
        Get
            Return _queueid
        End Get
        Set(value As Integer)
            _queueid = value
            OnPropertyChanged("Queueid")
        End Set
    End Property

    Public Property Sort As String
        Get
            Return _sort
        End Get
        Set(value As String)
            _sort = value
            OnPropertyChanged("Sort")
        End Set
    End Property

    Public Property Ausgabeverz As String
        Get
            Return _ausgabeverz
        End Get
        Set(value As String)
            _ausgabeverz = value
            OnPropertyChanged("Ausgabeverz")
        End Set
    End Property

    Public Property Lokal As String
        Get
            Return _lokal
        End Get
        Set(value As String)
            _lokal = value
            OnPropertyChanged("Lokal")
        End Set
    End Property


    Protected Overridable Sub OnPropertyChanged(ByVal propertyName As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Event PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

End Class

'Public Class AuftragEditMetaData

'    Public Property Application As String


'End Class
