﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class Auftrag
    Implements INotifyPropertyChanged

    Public _Aid As Long

    Public Adid As Long

    Private _vorlage As String

    Private _serviceStatus As String

    Private _offen As Boolean

    Private _druck As Boolean

    Private _massendruck As Boolean
    Private _archiv As Boolean
    Private _faxEmail As String

    Private _jobList As New ObservableCollection(Of Job) ' Wichtig NEw

    Private _bezeichnung As String

    Private _anzahl As Long = 0

    Private _zuArchivierende As Long = 0

    Private _fehler As Long = 0

    Private _mandant As String

    '1.0.3
    Private _application As String

    Private _lokal as String

    ', AuftragRow.KN_MASSENDRUCK = "Y", AuftragRow.KN_ARCHIV = "Y", AuftragRow.KN_FAX_EMAIL = "Y", AuftragRow.KN_GIFSTATUS
    '    ' Set the backing field so that you don't raise the 
    '    ' PropertyChanged event when you first create the Customer
    Public Sub New(CurAuftragDef As AuftragContext.GDAUFTRAGDEF, CurAuftrag As AuftragContext.GDAUFTRAG, Anzahl As Long, ToArchiv As Long, Fehler As Long)
        'Public Sub New(ADID As Long, Aid As Long, Vorlage As String, ServiceStatus As String, Offen As Boolean, Druck As Boolean, Massendruck As Boolean, Archiv As Boolean, FaxEmail As Boolean, LeasItStatus As String)
        Try
            With CurAuftrag
                Adid = .ADID
                _Aid = .AID
                _serviceStatus = .SERVICESTATUS
                _offen = .OFFEN = "Y"
                _druck = .KNDRUCK = "Y"
                _massendruck = .KNMASSENDRUCK = "Y"
                _archiv = .KNARCHIV = "Y"
                _faxEmail = .KNFAXEMAIL
                _lokal = .LOKAL

                'If Not .IsKN_GIFSTATUSNull Then
                '    _LeaseItStatus = .KN_GIFSTATUS
                'Else
                '    _LeaseItStatus = ""
                'End If

            End With

            With CurAuftragDef
                _vorlage = .VORLAGE
                _bezeichnung = .BEZEICHNUNG
                _mandant = .MANDANT
                _application = .APPLICATION

            End With


            _anzahl = Anzahl
            _zuArchivierende = ToArchiv
            _fehler = Fehler

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Property AID() As Long
        Get
            Return _Aid
        End Get
        Set(value As Long)
            _Aid = value
            OnPropertyChanged("AID")
        End Set
    End Property

    Public Property Vorlage() As String
        Get
            Return _Vorlage
        End Get
        Set(value As String)
            _Vorlage = value
            OnPropertyChanged("Vorlage")
        End Set
    End Property

    Public Property ServiceStatus As String
        Get
            Return _ServiceStatus
        End Get
        Set(value As String)
            _ServiceStatus = value
            ' ReadOnly !!!

            'Globals.GifDog.VW_TST_DOGMA_AUFTRAG_DEF_TST
            '(FindByPJOBID(_PJobId).Item(PropertyName) = _Template)
            OnPropertyChanged("ServiceStatus")
        End Set
    End Property

    Public Property JobList As ObservableCollection(Of Job)
        Get
            Return _JobList
        End Get
        Set(value As ObservableCollection(Of Job))
            _JobList = value
        End Set
    End Property

    Public Property Offen As Boolean
        Get
            Return _Offen
        End Get
        Set(value As Boolean)
            _Offen = value
            OnPropertyChanged("Offen")
        End Set
    End Property

    Public Property Druck As Boolean
        Get
            Return _Druck
        End Get
        Set(value As Boolean)
            _Druck = value
            OnPropertyChanged("Druck")
        End Set
    End Property

    Public Property Massendruck As Boolean
        Get
            Return _Massendruck
        End Get
        Set(value As Boolean)
            _Massendruck = value
            OnPropertyChanged("Massendruck")
        End Set
    End Property

    Public Property Archiv As Boolean
        Get
            Return _Archiv
        End Get
        Set(value As Boolean)
            _Archiv = value
            OnPropertyChanged("Archiv")
        End Set
    End Property

    Public Property FaxEmail As String
        Get
            Return _FaxEmail
        End Get
        Set(value As String)
            _FaxEmail = value
            OnPropertyChanged("FaxEmail")
        End Set
    End Property

   Public Property Bezeichnung As String
        Get
            Return _Bezeichnung
        End Get
        Set(value As String)
            _Bezeichnung = value
            OnPropertyChanged("Bezeichnung")
        End Set
    End Property

    Public Property Anzahl As Long
        Get
            Return _Anzahl
        End Get
        Set(value As Long)
            _Anzahl = value
            OnPropertyChanged("Anzahl")
        End Set
    End Property

    Public Property ZuArchivierende As Long
        Get
            Return _ZuArchivierende
        End Get
        Set(value As Long)
            _ZuArchivierende = value
        End Set
    End Property

    Public Property Fehler As Long
        Get
            Return _Fehler
        End Get
        Set(value As Long)
            _Fehler = value
        End Set
    End Property

    Public Property Mandant As String
        Get
            Return _Mandant
        End Get
        Set(value As String)
            _Mandant = value
        End Set
    End Property

    Public Property Application As String
        Get
            Return _application
        End Get
        Set(value As String)
            _application = value
        End Set
    End Property

    Public Property Lokal As String
        Get
            Return _lokal
        End Get
        Set(value As String)
            _lokal = value
            OnPropertyChanged("Lokal")
        End Set
    End Property

    ' TODO: Umstellung
    Protected Overridable Sub OnPropertyChanged(ByVal propertyName As String)
        Dim CurVal As String
        Select Case propertyName
            Case "ServiceStatus"
                ' Globals.GifDog.GD_AUFTRAG.FindByAID(_Aid).SERVICESTATUS = _serviceStatus
                Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).SERVICESTATUS = _serviceStatus
            Case ("Archiv")
                If _archiv Then CurVal = "Y" Else CurVal = "N"

                Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).KNARCHIV = CurVal
            Case "Druck"
                If _druck Then CurVal = "Y" Else CurVal = "N"
                Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).KNDRUCK = CurVal
            Case "Massendruck"

                If _massendruck Then CurVal = "Y" Else CurVal = "N"

                Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).KNMASSENDRUCK = CurVal
            Case "Offen"

                If _offen Then CurVal = "Y" Else CurVal = "N"

                Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).OFFEN = CurVal
            Case "FaxEmail"
                If _faxEmail = "N" Or _faxEmail = "F" Or _faxEmail = "E" Then

                    Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).KNFAXEMAIL = _faxEmail
                End If
            Case "Lokal"
                Globals.GifDogDb.GDAUFTRAGs.FirstOrDefault(Function(ds) ds.AID = AID).LOKAL = _lokal
        End Select

        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Event PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
End Class
