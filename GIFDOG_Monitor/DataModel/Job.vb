﻿Imports System.ComponentModel
Public Class Job
    Implements INotifyPropertyChanged


    Private _pJobId As Long

    Private _status As String
    Private _statusDatum As Date
    Private _fehlerStatus As String
    Private _fehlermeldung As String
    Private _erstellt As Date

    Public Sub New(PJobId As Long, Status As String, StatusDatum As Date, FehlerStatus As String, Fehlermeldung As String, Erstellt As Date)
        _pJobId = PJobId
        _status = Status
        _statusDatum = StatusDatum
        _fehlerStatus = FehlerStatus
        _fehlermeldung = Fehlermeldung
        _erstellt = Erstellt
    End Sub

    'Public Property JobName As String
    '    ' N
    '    Get
    '        Return _Name
    '    End Get
    '    Set(value As String)
    '        _Name = value
    '        OnPropertyChanged("JobName")
    '    End Set
    'End Property

    'Public Property Template As String
    '    Get
    '        Return _Template
    '    End Get
    '    Set(value As String)
    '        _Template = value
    '        ' Hier auch Update Möglich
    '        OnPropertyChanged("Template")
    '    End Set
    'End Property

    Public Property PJobId As Long
        Get
            Return _PJobId
        End Get
        Set(value As Long)

            _PJobId = value
            OnPropertyChanged("PJobId")
        End Set
    End Property

    Public Property Status As String
        Get
            Return _Status
        End Get
        Set(value As String)
            _Status = value
            OnPropertyChanged("Status")

        End Set
    End Property

    Public Property StatusDatum As Date
        Get
            Return _StatusDatum
        End Get
        Set(value As Date)
            _StatusDatum = value
            OnPropertyChanged("StatusDatum")

        End Set
    End Property

    Public Property FehlerStatus As String
        Get
            Return _FehlerStatus
        End Get
        Set(value As String)
            _FehlerStatus = value
            OnPropertyChanged("Fehlerstatus")

        End Set
    End Property

    Public Property Fehlermeldung As String
        Get
            Return _Fehlermeldung
        End Get
        Set(value As String)
            _Fehlermeldung = value
            OnPropertyChanged("Fehlermeldung")

        End Set
    End Property

    Public Property Erstellt As Date
        Get
            Return _Erstellt
        End Get
        Set(value As Date)
            _Erstellt = value
        End Set
    End Property

    Protected Overridable Sub OnPropertyChanged(ByVal propertyName As String)

        Select Case PropertyName
            Case "Status"
                'Globals.GifDog.GD_DOGMAJOBS.FindByPJOBID(_pJobId).STATUS = _status
                Globals.GifDogDb.GDDOGMAJOBs.FirstOrDefault(Function(ds) ds.PJOBID = _pJobId).STATUS = _status
            Case "Fehlerstatus"
                Globals.GifDogDb.GDDOGMAJOBs.FirstOrDefault(Function(ds) ds.PJOBID = _pJobId).FEHLERSTATUS = _fehlerStatus
                'Globals.GifDog.GD_DOGMAJOBS.FindByPJOBID(_pJobId).FEHLERSTATUS = _fehlerStatus
                'Case "Template"
                '    Globals.GifDog.GD_DOGMAJOBS.FindByPJOBID(_PJobId).Item(PropertyName) = _Template
        End Select
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub
    Public Event PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
End Class
