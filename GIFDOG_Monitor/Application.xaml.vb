﻿
Imports System.Windows
Partial Public Class Application
    'Inherits Application

    ' Application-level events, such as Startup, Exit, and DispatcherUnhandledException
    ' can be handled in this file.

    Protected Overrides Sub OnStartup(e As System.Windows.StartupEventArgs)

        'Utils.Log.Log4NetManager.Configure(New System.IO.FileInfo(My.Application.Info.DirectoryPath & "\Log4NetConf.xml"))
        'Utils.Log.Log4NetManager.AddAppender("DefaultLog", Utils.Log.Log4NetManager.CreateRollingFileAppender("DefaultLogFile", "GIFDOG_Monitor.log", True))
        'Utils.Log.Log4NetManager.SetLevel("DefaultLog", "ALL")

        'If Now > CDate("30.01.2013") Then
        '    MsgBox("DemoVersion")
        '    End
        'End If

        Globals.ShowLog("StartApplication")

        If Not OraDb.OpenDb() Then End ' Exit Sub

        Globals.ShowLog("Database opened")

  
        Try
            MyBase.OnStartup(e)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Application_DispatcherUnhandledException(ByVal sender As Object, ByVal e As System.Windows.Threading.DispatcherUnhandledExceptionEventArgs)
        MessageBox.Show(e.Exception.Message)
        End
    End Sub

    Protected Overrides Sub OnExit(e As System.Windows.ExitEventArgs)
        ' Globals.CurCmds.Dispose()
        OraDb.CloseDb()

        MyBase.OnExit(e)
    End Sub
End Class
