﻿Namespace MassenDruck
    Friend Class JobData
        Private _aid As Long
        Private ReadOnly _guid As String
        Private _template As String
        Private _daten As String

        'Private _mandant As String
        ' sind momentan noch VarChars auf der Profuktion
        Private _partnerid As String
        Private _geschnr As String
        Private _rechnr As String


        Private _mahnstufe As String
        Private _filiale As String
        Private _alpha As String

        Public Sub New()
            Dim g As Guid = New Guid
            g = System.Guid.NewGuid
            _guid = g.ToString()
        End Sub


        Public Property Aid As Long
            Get
                Return _aid
            End Get
            Set(value As Long)
                _aid = value
            End Set
        End Property

        Public ReadOnly Property Guid As String
            Get
                Return _guid
            End Get
        End Property

        Public ReadOnly Property Application As String
            Get
                Return "MASSENDRUCK"
            End Get
        End Property

        Public Property Template As String
            Get
                Return _template
            End Get
            Set(value As String)
                _template = value
            End Set
        End Property

        Public Property Daten As String
            Get
                Return _daten
            End Get
            Set(value As String)
                _daten = value
            End Set
        End Property

        'Public Property Mandant As String
        '    Get
        '        Return _mandant
        '    End Get
        '    Set(value As String)
        '        _mandant = value
        '    End Set
        'End Property

        Public Property Partnerid As String
            Get
                Return _partnerid
            End Get
            Set(value As String)
                _partnerid = value
            End Set
        End Property

        Public Property GeschNr As String
            Get
                Return _geschnr
            End Get
            Set(value As String)
                _geschnr = value
            End Set
        End Property

        Public Property Mahnstufe As String
            Get
                If String.IsNullOrEmpty(_mahnstufe) Then
                    Return "Null"
                Else
                    Return _mahnstufe
                End If

            End Get
            Set(value As String)
                _mahnstufe = value
            End Set
        End Property

        Public Property Filiale As String
            Get
                If String.IsNullOrEmpty(_filiale) Then
                    Return "Null"
                Else
                    Return _filiale
                End If

            End Get
            Set(value As String)
                _filiale = value
            End Set
        End Property

        Public Property Alpha As String
            Get
                Return _alpha
            End Get
            Set(value As String)
                _alpha = value
            End Set
        End Property

        Public Property RechNr As String
            Get
                ' Für den OraInsert erforderlich
                If String.IsNullOrEmpty(_rechnr) Then
                    Return "Null"
                Else
                    Return _rechnr
                End If

            End Get
            Set(value As String)
                _rechnr = value
            End Set
        End Property
    End Class
End Namespace