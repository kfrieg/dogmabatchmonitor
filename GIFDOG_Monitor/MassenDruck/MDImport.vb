﻿Imports System.Data
Imports System.Text

Namespace MassenDruck

    Friend Class MDImport
        Private Const startInsert As String = "insert into gd_dogmajobs (aid,  guid,  application,  template,  daten,  erstellt,  status,  statusdatum,  fehlerstatus,  fehlermeldung,  geschnr,  partnerid,  rechnr,  mandant,  mahnstufe,  filiale,  alpha ) "
        Public Function Import(ByVal curAid As Long, ByVal curTemplate As String, ByVal curMandant As String) As Boolean

            ' Template Abfragen ' ' AID ermitteln
            ' Selektierter Auftrag ??
            ' MsgBox(CurAid & CurTemplate)


            ' Datei öffen
            Dim dlg As New Microsoft.Win32.OpenFileDialog()
            dlg.DefaultExt = ".xlsx"
            dlg.Filter = "Excel Dateien (.xlsx)|*.xlsx"
            Dim result As Nullable(Of Boolean) = dlg.ShowDialog()
            Dim LogMessage As New Stringbuilder
            Dim filename As String

            ' Konvertieren
            If result = True Then

                Try

                    ' Open document 
                    filename = dlg.FileName
                    Dim ExcelImp As New ExcelImport(filename, curAid, curTemplate)
                    Try

                        ExcelImp.GenerateData()

                        If ExcelImp.RowErrors.ToString() <> "" Then
                            Dim Showerrors As String = String.Format("Fehler in folgenden Zeilen:{0}{1}", vbCrLf, ExcelImp.RowErrors.ToString().Substring(0, 500))
                            ' 1.0.5  Log
                            LogMessage.AppendLine(Showerrors)
                            MsgBox(Showerrors, MsgBoxStyle.Critical)
                        End If

                    Catch ex As Exception
                        ' 1.0.5 Log
                        LogMessage.AppendLine(String.Format("Import-Fehler: {0}", ex.Message))
                        MsgBox(String.Format("Import-Fehler: {0}", ex.Message))
                        SaveLogMessage(filename, LogMessage)
                        Return False
                    End Try

                    ' Sicherheits Abfrage
                    Dim sAbfrage As String = String.Format("Sollen {0} Jobs für den Auftrag {1} - {2} erzeugt werden?", ExcelImp.JobDataList.Count(), curAid, curTemplate)
                    If ExcelImp.JobDataList.Count() AndAlso MsgBox(sAbfrage, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then


                        Dim OraCmd As New Devart.Data.Oracle.OracleCommand()
                        Try
                            OraCmd.Connection = Globals.Conn
                            OraCmd.CommandTimeout = 1200
                            OraCmd.CommandType = CommandType.Text

                            ' Speichern in DB
                            For Each CurJobData As JobData In ExcelImp.JobDataList

                                ' 1.0.5 Exception
                                Try
                                    With CurJobData
                                        OraCmd.CommandText = String.Format("{0} values ({1},'{2}','{3}','{4}',q'#{5}#',sysdate,'A',sysdate,'N','','{6}','{7}',{8},'{9}',{10},{11},'{12}')", _
                                                                           startInsert, curAid, .Guid, "Massendruck", curTemplate, .Daten, .GeschNr, .Partnerid, .RechNr, curMandant, .Mahnstufe, .Filiale, .Alpha)

                                        ' TODO: Ggf. Batch execute
                                        OraCmd.ExecuteNonQuery()
                                        'GifDog.GD_DOGMAJOBS.AddGD_DOGMAJOBSRow(PjobId, CurAuftragRow, .Guid, "MASSENDRUCK", CurTemplate, Now, "A", Now, "N", "", "", "", 0, "", 0, 0, "")
                                        'PjobId += 1
                                    End With
                                Catch ex As Exception
                                    ' 1.0.5  Log
                                    Dim ShowErrors As String = String.Format("Oracle-Insert-Fehler: {0} - {1}", ex.Message, OraCmd.CommandText)
                                    LogMessage.AppendLine(ShowErrors)
                                    MsgBox(ShowErrors, MsgBoxStyle.Critical)
                                    SaveLogMessage(filename, LogMessage)
                                    Return False
                                End Try

                            Next
                            'JobsTa.Update(GifDog)
                            ExcelImp.JobDataList.Clear()
                            OraCmd.Dispose()
                        Catch ex As Exception
                            ' 1.0.5  Log
                            Dim ShowErrors As String = String.Format("Oracle-Fehler: {0} - {1}", ex.Message, OraCmd.CommandText)
                            LogMessage.AppendLine(ShowErrors)
                            MsgBox(ShowErrors, MsgBoxStyle.Critical)
                            SaveLogMessage(filename, LogMessage)
                            Return False
                        End Try
                    Else
                        MsgBox("Keine Datensätze erzeugt!", MsgBoxStyle.Information)
                        SaveLogMessage(filename, LogMessage)
                        Return False
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical)
                    SaveLogMessage(filename, LogMessage)
                    Return False
                End Try


                Return True
            Else
                Return False
            End If





        End Function

        Private Sub SaveLogMessage(ByVal filename As String, ByVal LogMessage As StringBuilder)

            ' 1.0.5 Meldungen ausgeben
            If LogMessage.Length > 0 Then
                Dim Curdir As String = My.Computer.FileSystem.GetFileInfo(filename).Directory.FullName
                Dim CurFileName As String = My.Computer.FileSystem.GetFileInfo(filename).Name
                My.Computer.FileSystem.WriteAllText(IO.Path.Combine(Curdir, CurFileName & "_Error.txt"), LogMessage.ToString(), False)
            End If
        End Sub
    End Class
End Namespace