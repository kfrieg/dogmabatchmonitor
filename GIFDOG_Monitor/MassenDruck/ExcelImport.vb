﻿Imports System.Text
Imports LinqToExcel
Imports LinqToExcel.Query

Namespace MassenDruck

    Friend Class ExcelImport
        Private ReadOnly _filename As String
        Private ReadOnly _aid As Long
        Private ReadOnly _template As String

        Private ReadOnly _jobDataList As New List(Of JobData)
        Private ReadOnly _colList As New List(Of String)

        Public RowErrors As New StringBuilder


        Public Sub New(filename As String, aid As Long, template As String)
            _filename = filename
            _aid = aid
            _template = template
        End Sub

        Public ReadOnly Property JobDataList As List(Of JobData)
            Get
                Return _jobDataList
            End Get
        End Property

        Public Sub GenerateData()
            Dim ColIndex As Long
            Dim RowIndex As Long = 1
            Dim RowError As Boolean

            Dim book = New LinqToExcel.ExcelQueryFactory(_filename)


            Dim ListOfWs As List(Of String) = book.GetWorksheetNames()

            ' 1.0.5
            Dim ws As ExcelQueryable(Of Row)
            If ListOfWs.Contains("Massendruck") Then
                ws = book.Worksheet("Massendruck")
            Else
                ws = book.Worksheet(ListOfWs(0))
            End If

            For Each col In ws.FirstOrDefault.ColumnNames
                ' 1.0.5
                _colList.Add(col.Replace(" ", "_").Replace("<", "").Replace(">", "").Replace("&", "_").Replace("#", "_"))
            Next


            Dim CurElements As New DataElements ' Elements


            '_colList.AddRange(ws.FirstOrDefault.ColumnNames)

            For Each r As LinqToExcel.Row In ws
                ColIndex = 0



                Dim CurJobData As New JobData

                Dim XmlDoc As New XDocument
                XmlDoc.Declaration = New XDeclaration("1.0", "utf-8", "yes")
                Dim XmlEl As New XElement(DataElements.DataRoot)
                XmlEl.Add(New XElement(DataElements.PartnerRoot, ""))
                XmlEl.Add(New XElement(DataElements.GeschaeftRoot, ""))
                XmlEl.Add(New XElement(DataElements.MailRoot, ""))

                Try
                    For Each c As LinqToExcel.Cell In r
                        'Console.WriteLine(_colList(ColIndex) & "_" & c.Value & "-" & c.Value.GetType.ToString())
                        Dim ColName As String = _colList(ColIndex)
                        Dim CurValue As String = GetCurValue(c)

                        Dim CurEls = CurElements.Where(Function(e) e.ColName = LCase(ColName)).ToList()
                        If CurEls.Any() Then
                            Dim CurEl As DataElement = CurEls.First()
                            If CurEl.XRoot <> "" Then
                                XmlEl.Element(CurEl.XRoot).Add(New XElement(CurEl.XName, CurValue))
                            Else
                                XmlEl.Add(New XElement(CurEl.XName, CurValue))
                            End If
                            If CurEl.JobDataName <> "" Then
                                CurJobData.GetType().GetProperty(CurEl.JobDataName).SetValue(CurJobData, CurValue, Nothing)
                            End If
                        Else
                            XmlEl.Add(New XElement(ColName, CurValue))
                        End If
                        ColIndex += 1
                    Next
                    RowError = False
                    RowIndex += 1
                Catch ex As Exception
                    ' Row-Error
                    RowError = True
                    RowErrors.Append(String.Format("Fehler Zeile {0} - {1}{2}", RowIndex, ex.Message, vbCrLf))
                End Try

                If Not RowError Then
                    XmlDoc.Add(XmlEl)
                    With CurJobData
                        .Aid = _aid
                        .Template = _template
                        .Daten = XmlDoc.Declaration.ToString() & vbCrLf & XmlDoc.ToString()
                    End With

                    _jobDataList.Add(CurJobData)

                End If

            Next
        End Sub

        Private Shared Function GetCurValue(ByVal c As LinqToExcel.Cell) As String
            Dim CurValue As String
            Select Case c.Value.GetType
                Case GetType(System.DBNull)
                    CurValue = ""
                Case GetType(System.DateTime)
                    CurValue = CDate(c.Value).ToShortDateString()
                Case GetType(System.Double), GetType(System.Decimal) ' ggf. TausenderPunkt
                    CurValue = c.Value.ToString()
                Case Else
                    CurValue = c.Value
            End Select
            Return CurValue
        End Function

    End Class
End Namespace