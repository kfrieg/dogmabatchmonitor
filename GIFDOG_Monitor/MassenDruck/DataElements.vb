﻿Namespace MassenDruck

    Friend Class DataElements
        Inherits List(Of DataElement)

        Public Const PartnerRoot As String = "Partner"
        Public Const GeschaeftRoot As String = "Geschaeft"
        Public Const MailRoot As String = "Mail"
        Public Const DataRoot As String = "Data"

        Public Sub New()
            ' 1.3.0 Nicht aus den Daten
            '            Add(New DataElement() With {.ColName = "mandant", .XName = "Mandant", .XRoot = "", .JobDataName = "Mandant"})

            Add(New DataElement() With {.ColName = "partnerid", .XName = "Id", .XRoot = PartnerRoot, .JobDataName = "Partnerid"})
            Add(New DataElement() With {.ColName = "partnername1", .XName = "Name1", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "partnername2", .XName = "Name2", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "partnername3", .XName = "Name3", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "partnername", .XName = "Name", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "alpha", .XName = "Alpha", .XRoot = PartnerRoot, .JobDataName = "Alpha"})
            Add(New DataElement() With {.ColName = "anrede", .XName = "Anrede", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "landname", .XName = "Landname", .XRoot = PartnerRoot, .JobDataName = ""})

            Add(New DataElement() With {.ColName = "ort", .XName = "Ort", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "plz", .XName = "Plz", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "strasse", .XName = "Strasse", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "land", .XName = "Land", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "faxnummer", .XName = "Faxnummer", .XRoot = PartnerRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "filiale", .XName = "Filiale", .XRoot = PartnerRoot, .JobDataName = "Filiale"})

            Add(New DataElement() With {.ColName = "geschnr", .XName = "GeschNr", .XRoot = GeschaeftRoot, .JobDataName = "GeschNr"})
            Add(New DataElement() With {.ColName = "rechnr", .XName = "RechNr", .XRoot = GeschaeftRoot, .JobDataName = "RechNr"})
            Add(New DataElement() With {.ColName = "vertragsart", .XName = "Vertragsart", .XRoot = GeschaeftRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "mahnstufe", .XName = "Mahnstufe", .XRoot = GeschaeftRoot, .JobDataName = "Mahnstufe"})

            Add(New DataElement() With {.ColName = "mailanrede", .XName = "MailAnrede", .XRoot = MailRoot, .JobDataName = ""})


            Add(New DataElement() With {.ColName = "sendername", .XName = "MailFromName", .XRoot = MailRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "senderadresse", .XName = "MailFromAddress", .XRoot = MailRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "empfängername", .XName = "MailReceiverName", .XRoot = MailRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "empfängeradresse", .XName = "MailReceiverAddress", .XRoot = MailRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "mailbetreff", .XName = "MailSubject", .XRoot = MailRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "mailtext", .XName = "MailMessageText", .XRoot = MailRoot, .JobDataName = ""})
            Add(New DataElement() With {.ColName = "anhangname", .XName = "MailAppendixFileName", .XRoot = MailRoot, .JobDataName = ""})

        End Sub
    End Class
End Namespace