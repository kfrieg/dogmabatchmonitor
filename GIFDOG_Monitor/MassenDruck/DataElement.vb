﻿Namespace MassenDruck
    Friend Class DataElement

        Public Property ColName As String
        Public Property XRoot As String
        Public Property XName As String
        Public Property JobDataName As String

        Public Sub New()

        End Sub

        Public Sub New(cColName As String, cXRoot As String, cXName As String, cJobDataName As String)
            ColName = cColName
            XName = cXName
            XRoot = cXRoot
            JobDataName = cJobDataName
        End Sub

    End Class
End Namespace