﻿Imports System.ComponentModel
Imports Syncfusion.Windows.Shared
Imports Syncfusion.Windows.Controls.Grid
Imports DogmaBatch_Monitor.Globals

Partial Public Class MainWindow
    Inherits ChromelessWindow
    Public Sub New()
        'System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("de-DE")
        Try
            Globals.Conn = OraDb.Conn

            Globals.Auftraege = New AuftragColl
            Globals.ShowLog("Datenklassen geladen")

        Catch ex As Exception
            MsgBox("Data: " & ex.Message)
        End Try


        Try
            ' Muss Vor Init zugewiesen werdne
            Me.DataContext = Globals.Auftraege
        Catch ex As Exception
            MsgBox("Data2: " & ex.Message)
        End Try



        Try
            ' This call is required by the designer.
            Globals.ShowLog("Window init")
            InitializeComponent()
            Globals.ShowLog("After Window init")
            ' Darf erst nach Init ZUgewiesen werden
        Catch ex As Exception
            MsgBox("Init: " & ex.Message)
        End Try

        CurCmds.Grid = Me.DCGrid



        Try
            Dim dropdown As Syncfusion.Windows.Controls.Grid.GridDataVisibleColumn
            dropdown = Me.DCGrid.VisibleColumns(8)
            Dim Dlist = New List(Of DropList)

            ' 1.0.8
            Dlist.Add(New DropList With {.Display = "Aktiv", .Value = "A"})
            ' Dlist.Add(New DropList With {.Display = "Lokal aktiv", .Value = "L"})
            Dlist.Add(New DropList With {.Display = "Nicht aktiv", .Value = "N"})
            dropdown.ColumnStyle.ItemsSource = Dlist

            dropdown.ColumnStyle.DisplayMember = "Display"
            dropdown.ColumnStyle.ValueMember = "Value"
            dropdown.ColumnStyle.ShowDropDownHeaders = False
            dropdown.ColumnStyle.DropDownStyle = GridDropDownStyle.Exclusive

            ' Update über RowID möglich
            ' StartDepTracking()

            ' AddHandler Globals.Auftraege.
        Catch ex As Exception
            MsgBox("Init2: " & ex.Message)
        End Try
    End Sub


    Private Sub MouseDoubleClick(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles DCGrid.MouseDoubleClick
        If Not mnuEdit.IsChecked Then
            Globals.CurCmds.Execute("EditAuftrag")
        End If


    End Sub

    Private Sub DCGrid_CurrentCellValidated(sender As Object, args As Syncfusion.Windows.ComponentModel.SyncfusionRoutedEventArgs) Handles DCGrid.CurrentCellValidated
        CurControls.ToSave = True
    End Sub

    Private Sub DCGrid_RecordsSelectionChanged(sender As Object, e As Syncfusion.Windows.Controls.Grid.GridDataRecordsSelectionChangedEventArgs) Handles DCGrid.RecordsSelectionChanged
        ' Nur die Erste Ebene
        Try
            Dim _CurAuftrag As Auftrag = DirectCast(e.AddedItems(0), Auftrag)

            CurAuftrag = _CurAuftrag
            With _CurAuftrag
                CurAid = ._Aid
                ' 1.0.3
                CurTemplate = .Vorlage
                CurApplication = .Application
                CurMandant = .Mandant
            End With
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub StartDepTracking()
    '    ' ACHTUNG: FireWall

    '    ' Open the connection
    '    Dim connection = New OracleConnection("Server = Ora102; User Id = GIF_DOG; Password = GIF_DOG;")
    '    connection.Open()

    '    ' Create the Select command retrieving all data from the Dept table.
    '    Dim selectCommand = New OracleCommand("select pjobid from gd_dogmajobs", connection)

    '    ' Create an OracleDependency object and set it to track the resul set returned by selectCommand.
    '    Dim dependency As OracleDependency = New OracleDependency(selectCommand)


    '    'Setting object-based change notification registration
    '    dependency.QueryBasedNotification = False

    '    ' When the IsNotifiedOnce property is true, only the first change
    '    ' of the traced result set will generate a notification.
    '    ' Otherwise, notifications will be sent on each change
    '    ' during the selectCommand.Notification.Timeout period.
    '    selectCommand.Notification.IsNotifiedOnce = False 'True
    '    ' Specifies whether notifications will contain information on rows changed.
    '    selectCommand.Notification.RowLevelDetails = True

    '    ' Set the event handler to the OnChange event.
    '    AddHandler dependency.OnChange, AddressOf OnDeptChange

    '    ' When the select command is executed at the first time, a notification
    '    ' on changes of the corresponding result set is registered on the server.
    '    selectCommand.ExecuteReader()

    '    '' Set and execute an insert command. The Dept table data will be changed,
    '    '' and a notification will be sent, causing the OnChange event of the 'dependency' object.
    '    'Dim insertCommand = New OracleCommand( _
    '    '    "insert into dept values (100, 'New department', 'Some location')", connection)
    '    'insertCommand.ExecuteNonQuery()

    '    ' Pause the current thread to process the event.
    '    'Thread.Sleep(10000)
    'End Sub
    'Private Sub OnDeptChange(ByVal sender As Object, ByVal args As OracleNotificationEventArgs)

    '    ' Malö Alles


    '    ' JobsTa.Fill(GifDog.GD_DOGMAJOBS)

    '    'AuftragsTa.Fill(GifDog.VW_TST_DOGMA_AUFTRAG_DEF_TST)
    '    Dim dt As DataTable = args.Details

    '    Console.WriteLine("The following database objects were changed:")
    '    For Each resource As String In args.ResourceNames
    '        Console.WriteLine(resource)
    '    Next

    '    Console.WriteLine(vbCrLf + "Details: ")
    '    Console.Write(New String("*", 80))

    '    ' rowid über dt.Rows(rows).ItemArray(2)

    '    For rows As Integer = 0 To dt.Rows.Count - 1
    '        Console.WriteLine("Resource name: " + dt.Rows(rows).ItemArray(0))
    '        Dim type As String = [Enum].GetName( _
    '            GetType(OracleNotificationInfo), dt.Rows(rows).ItemArray(1))
    '        Console.WriteLine("Change type: " + type)
    '        Console.Write(New String("*", 80))
    '    Next
    'End Sub



    Protected Overrides Sub OnClosed(e As EventArgs)
        Globals.CurCmds.Dispose()
        OraDb.CloseDb()

        MyBase.OnClosed(e)

        ' Wichtig - ansosnten bleibt er beim Schließen über X hängen
        End
    End Sub
    


End Class
