﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Public MustInherit Class ErrorInfoClass
    ' Diese Klasse dient nur dazu um die IDataErrorInfo auch in den ViewModels zur Verfügung zu haben
    ' Dies ist wiederum nur Nötig da die neumannsche Dataform beim ersten init keine ErrorInfos anzeigt - beim wiederholten schon
    ' Ist auch nur für AddNew wichtig

    Implements IDataErrorInfo

    <Display(AutoGenerateField:=False)>
    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            Return Me("")
        End Get
    End Property

   
    Default Public Overridable ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get

        End Get
    End Property
End Class